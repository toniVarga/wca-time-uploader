# WCA Time Uploader

Using ESP32 we plan to create a piece of hardware that hooks up to your stackmat timer, picks up the measured time from it and uploads it to database (e.g. cubecomps).
The device should completely bypass the procedure of writing measured times by hand and then typing them to the database, thus reducing errors in the process and releasing
the delegates from a lot of boring and time consuming work.

The procedure will be as follows:
1.  Each competitor is given an NFC tag at the beginning of the competition. NFC tag contains information about competitor (first and last name, wca ID...)
      and it is programmed at the registrations at the venue.
2.  Before cube inspection, the competitor will sign in to solve by sliding the tag near the device that is connected to stackmat the same way the competiton display
    hooks up (using audio cable). This way information about current competitor is acquired and shown on the devices display and the solve can begin.
3.  The competitor solves the cube and the time is picked up by device (and shown on devices display).
4.  The judge inspects the cube and defines "DNF", "PENALTY" or "OK" using the navigation joystick on the device.
5.  Competitor signs out of solve and the time is uploaded to database along with all relevant information about the solve.

Information about the discipline and round is configured by the judge on device using navigation joystick before each round.

Components:
    ESP32 module - The microcontroler that serves as a brain of the device: controls the peripherals and uploads the time.
    MFRC522 - NFC Reader which will enable signin in and out of solves. Connects to the ESP32 using SPI.
    2x16 character LCD display - To show the menus, time and other information. Connects to the ESP32 using I2C.
    Analog joystick - To navigate the menus and options on the display.
    MAX232 - Integrated circuit that converts RS232 signals from the stackmat to UART signals for the microcontroler. (So we can read the time from the stackmat).